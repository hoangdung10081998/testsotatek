import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TaskService } from '../../service/task.service';
import { ITask } from '../../model/itask';

@Component({
  selector: 'app-todo-update',
  templateUrl: './todo-update.component.html',
  styleUrls: ['./todo-update.component.css']
})
export class TodoUpdateComponent implements OnInit {

  frmTask: FormGroup;
  min = this.getDate();
  taskId: number;
  task: ITask = <any>{};
  constructor(private router: Router, private route: ActivatedRoute, private taskService: TaskService) { }

  ngOnInit() {
    this.frmTask = new FormGroup({
      nameTask: new FormControl(this.task.nameTask, Validators.compose([Validators.required])),
      description: new FormControl(this.task.description, Validators.required),
      dueDate: new FormControl(this.task.dueDate, Validators.compose([Validators.required])),
      priority: new FormControl(this.task.priority, Validators.required)
    });
    this.getTaskWithUrl();
  }

  get formValue() { return this.frmTask.controls; }

  getTaskWithUrl() {
    this.taskId = +this.route.snapshot.paramMap.get('id');
    const taskOsv$ = this.taskService.getOsvTaskById(this.taskId).subscribe(
      val => {
        try {
          console.log(val);
          this.task = val;
          this.formValue.nameTask.setValue(this.task.nameTask);
          this.formValue.description.setValue(this.task.description);
          this.formValue.dueDate.setValue(this.task.dueDate);
          this.formValue.priority.setValue(this.task.priority);
          this.formValue.isDone.setValue(this.task.isDone);
          this.formValue.showDetail.setValue(this.task.showDetail);
        } catch (error) {
          console.log(error);
          console.log(val);

        }
      }
    );
  }

  getDate() {
    let today = new Date();
    let dd = String(today.getDate()).padStart(2, '0');
    let mm = String(today.getMonth() + 1).padStart(2, '0');
    let yyyy = today.getFullYear();

    let dueDate = yyyy + '-' + mm + '-' + dd;
    return dueDate;
  }

  onSubmit() {
    if (this.frmTask.invalid) {
      return;
    }

    if (this.taskId !== 0) {
      const task: ITask = <any>{};
      task.id = this.taskId;
      task.nameTask = this.formValue.nameTask.value;
      task.description = this.formValue.description.value;
      task.dueDate = this.formValue.dueDate.value;
      task.priority = this.formValue.priority.value;

      const updateTask$ = this.taskService.updateTask(this.taskId, task);
      console.log(this.frmTask.controls.nameTask.value);

      updateTask$.subscribe(
        val => {
          try {
            console.log(val);
            this.router.navigate(['/todo-list']);
          } catch (error) {
            console.log(error);
          }
        }
      );
    }
    console.log(this.formValue);
  }

}
