import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import {formatDate} from '@angular/common';
import { TaskService } from '../../service/task.service';
import { ITask } from '../../model/itask';

@Component({
  selector: 'todo-create',
  templateUrl: './todo-add.component.html',
  styleUrls: ['./todo-add.component.css']
})
export class TodoAddComponent implements OnInit {

  frmTask: FormGroup;
  min = this.getDate();
  constructor(private router: Router, private taskService: TaskService) {}

  ngOnInit() {
    this.frmTask = new FormGroup({
      nameTask: new FormControl(null, Validators.compose([Validators.required])),
      description: new FormControl(null),
      dueDate: new FormControl(this.getDate(), Validators.compose([Validators.required])),
      priority: new FormControl('Normal', Validators.required)
    });
  }

  get formValue() {
    return this.frmTask.controls;
  }

  getDate() {
    let today = new Date();
    let dd = String(today.getDate()).padStart(2, '0');
    let mm = String(today.getMonth() + 1).padStart(2, '0');
    let yyyy = today.getFullYear();

    let dueDate = yyyy + '-' + mm + '-' + dd;
    return dueDate;
  }
  onSubmit() {
    if (this.frmTask.invalid) {
      return;
    }
    const task: ITask = <any>{};
    task.id = +this.getRandomInt(100, 1000);
    task.nameTask = this.formValue.nameTask.value;
    task.isDone = false;
    task.showDetail = false;
    task.description = this.formValue.description.value;
    task.dueDate = this.formValue.dueDate.value;
    task.priority = this.formValue.priority.value;
    const createTask$ = this.taskService.addTask(task);
    createTask$.subscribe(
      val => {
        try {
          console.log(val);
          this.router.navigate(['/todo-list']);
        } catch (error) {
          console.log(error);
        }
      }
    );
    console.log(this.frmTask.controls.nameTask.value);
  }

  getRandomInt(min: number, max: number): number {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }
}
