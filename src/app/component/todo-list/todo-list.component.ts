import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TaskService } from '../../service/task.service';
import { ITask } from '../../model/itask';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit {
  tasks: ITask[] = [];
  task: ITask = <any>{};
  today: number = Date.now();
  taskId: number;
  searchText: string;
  constructor(private router: Router, private taskService: TaskService) { }


  ngOnInit() {
    this.prepareData();

  }

  private prepareData() {
    const taskOsv$ = this.taskService.getOsvAllTask();
    taskOsv$.subscribe(
      val => {
        try {
          this.tasks = val;
        } catch (error) {
          console.log(error);
        }
      }
    );
  }

  get sortData() {
    return this.tasks.sort((a, b) => {
      return <any>new Date(a.dueDate) - <any>new Date(b.dueDate);
    });
  }

  deleteTask(id: number) {
    console.log(this.sortData);
    if (confirm('Are you sure you want to delete this task?')) {
      this.taskService.deleteTaskById(id).subscribe(
        val => {
          console.log(val);
          this.prepareData();
        }
      );
    } else {
      // Do nothing!
    }
  }
  showBulkAction() {
    // this.task.showDetail = !this.task.showDetail;
    console.log(this.task.showDetail);
  }
  addNewTask() {
    this.router.navigate(['/todo-create']);
  }

}
