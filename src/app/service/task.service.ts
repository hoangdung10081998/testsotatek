import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs';
import { ITask } from '../model/itask';

@Injectable({
  providedIn: 'root'
})
export class TaskService {
  baseUrl: string;
  constructor(private httpClient: HttpClient) {
    this.baseUrl = 'http://127.0.0.1:3000/';
  }

  private getHeaders() {
    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json');
    return headers;
  }
  getOsvAllTask(): Observable<ITask[]> {
    return this.httpClient.get<ITask[]>(this.baseUrl + 'task', { headers: this.getHeaders() });
  }

  getOsvTaskById(id: number): Observable<ITask> {
    return this.httpClient.get<ITask>(this.baseUrl + 'task/' + id, { headers: this.getHeaders() });
  }

  addTask(task: ITask): Observable<ITask> {
    return this.httpClient.post<ITask>(this.baseUrl + 'task', task, { headers: this.getHeaders() });
  }

  updateTask(id: number, task: ITask): Observable<ITask> {
    return this.httpClient.put<ITask>(this.baseUrl + 'task/' + id, task, { headers: this.getHeaders() });
  }

  deleteTaskById(id: number): Observable<ITask>{
    return this.httpClient.delete<ITask>(this.baseUrl + 'task/' + id, {headers: this.getHeaders()});
  }
}
