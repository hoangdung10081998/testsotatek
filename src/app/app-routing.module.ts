import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TodoAddComponent } from './component/todo-add/todo-add.component';
import { TodoListComponent } from './component/todo-list/todo-list.component';
import { TodoUpdateComponent } from './component/todo-update/todo-update.component';


const routes: Routes = [
  {path: '', component:TodoListComponent},
  {path: 'todo-list', component:TodoListComponent},
  {path: 'todo-create', component:TodoAddComponent},
  {path: 'todo-update/:id', component:TodoUpdateComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
