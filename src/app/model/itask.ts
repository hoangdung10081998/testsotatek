export interface ITask{
    id: number;
    isDone: boolean;
    showDetail: boolean;
    nameTask: string;
    description: string;
    dueDate: Date;
    priority: string;

}